<!-- Button trigger modal -->
  <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/bootstrap.css') }}">
  <link rel="shortcut icon" type="image/x-icon" href="{{ URL::asset('images/favicon.ico') }}">
  <link type="text/css" rel="stylesheet" href="css/bootstrap.css" />
  <script src="http://code.jquery.com/jquery.js"></script>
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> 
  <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>

<script type="text/javascript">
<script>
// JavaScript popup window function
  function basicPopup(url) {
popupWindow = window.open(url,'popUpWindow','height=500,width=500,left=100,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no, status=yes')
  }

</script>

<div class="container"> 
    <br>
  <button class="btn btn-primary btn-lg" href="#signup" data-toggle="modal" data-target=".bs-modal-sm">Sign In/Register</button>
  </center>
  <br>
 </div>
  

<!-- Modal -->
<div class="modal fade bs-modal-sm" id="myModal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
        <br>
        <div class="bs-example bs-example-tabs">
            <ul id="myTab" class="nav nav-tabs">
              <li class="active"><a href="#signin" data-toggle="tab">Sign In</a></li>
              <li class=""><a href="#signup" data-toggle="tab">Register</a></li>
              <li class=""><a href="#anonymous" data-toggle="tab">Anonymous</a></li>
            </ul>
        </div>
      <div class="modal-body">
        <div id="myTabContent" class="tab-content">
        <div class="tab-pane fade in" id="anonymous">
          <h1>Anonymous access</h1>
        <p>Continue as Anonymous user. No personal information will be requested.</p>

        <p></p><br>By pressing the button below you adhere to our anonymous usage <a href="/policy" onclick="basicPopup(this.href);return false">policy</a></p>
        
        {{ Form::open(array('url' => 'doAnonymous')) }}

        <p>{{ Form::submit('Submit') }}</p> 
        </div>
        <div class="tab-pane fade active in" id="signin">
          
             {{ Form::open(array('url' => 'login')) }}
    <h1>Sign In</h1>

    <!-- if there are login errors, show them here -->
    <p>
      {{ $errors->first('email') }}
      {{ $errors->first('password') }}
    </p>

    <p>
      {{ Form::label('email', 'Email:') }}</p><p>
      {{ Form::text('email', Input::old('email'), array('placeholder' => 'richard@matieswiki.com')) }}
    </p>

    <p>
      {{ Form::label('password', 'Password:') }}</p>
      {{ Form::password('password', array('placeholder' => '********')) }}<p>
    </p>   
    <p>{{ Form::submit('Submit') }}</p>
  {{ Form::close() }}
    
        </div>
        <div class="tab-pane fade" id="signup">
            
    {{ Form::open(array('url' => 'register')) }}
    <h1>Register</h1>
        {{ Form::label('Username:') }}
        
 
        <p>{{ Form::text('username') }}</p>
 
        {{ Form::label('Email:') }}
 
        <p>{{ Form::text('email') }}</p>
 
        {{ Form::label('Password:') }}
 
        <p>{{ Form::password('password') }}</p>

        <p>{{ Form::submit('Submit') }}</p> 

 
    {{ Form::close() }}
      </div>

    </div>
      </div>
      <div class="modal-footer">
      <center>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </center>
      </div>
    </div>
  </div>
</div>