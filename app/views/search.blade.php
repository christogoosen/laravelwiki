<!DOCTYPE html>
<html>
<head>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> 
<!--<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>-->
  <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/bootstrap.css') }}">
  <link rel="shortcut icon" type="image/x-icon" href="{{ URL::asset('images/favicon.ico') }}">
  <link type="text/css" rel="stylesheet" href="css/bootstrap.css" />
  <link type="text/css" rel="stylesheet" href="css/bootstrap.min.css" />
  <script src="http://code.jquery.com/jquery.js"></script>
  <!--<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> -->
  <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
  <script type="text/javascript">
  <script>
// JavaScript popup window function
  function basicPopup(url) {
popupWindow = window.open(url,'popUpWindow','height=500,width=500,left=100,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no, status=yes')
  }

</script>
  <title>Maties Wiki</title>
  <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/bootstrap.css') }}">
  <link rel="shortcut icon" type="image/x-icon" href="{{ URL::asset('images/favicon.ico') }}">
<style>
a:hover {
    color: black;
}

a {
  color: #730022;
}
</style>


</head>
<body class="image">

<div class="container">
  <nav class="navbar navbar-inverse">
  <div class="navbar-header">
    <a href="{{ URL::to('wiki') }}"><img src="Leaf_maroon_transparent.png" height="45" width="55" valig="center" align="left"></a>
    <a class="navbar-brand" href="{{ URL::to('wiki') }}">Home</a>
    <a class="navbar-brand" href="{{ URL::to('wiki/create') }}">Create Page</a>
  </div>  

    <div class="float-right">
     <a class="navbar-brand">{{ Form::open(array('url' => 'search')) }}
       {{Form::text('keyword', null, array('placeholder'=>'search by keyword'))}}
       {{Form::submit('search')}}
         {{Form::close()}}</a>
    <a class="navbar-brand" href="#signup" data-toggle="modal" data-target=".bs-modal-sm">Sign In/Register</a>
@if (isset(Auth::user()->username))
    <a class="navbar-brand" href="{{ URL::to('logout') }}">Logout</a>
    @endif
  </div>
  
</nav>
</div>

<!-- Modal -->
<div class="modal fade bs-modal-sm" id="myModal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
        <br>
        <div class="bs-example bs-example-tabs">
            <ul id="myTab" class="nav nav-tabs">
              <li class="active"><a href="#signin" data-toggle="tab">Sign In</a></li>
              <li class=""><a href="#signup" data-toggle="tab">Register</a></li>
              <li class=""><a href="#anonymous" data-toggle="tab">Anonymous</a></li>
            </ul>
        </div>
      <div class="modal-body">
        <div id="myTabContent" class="tab-content">
        <div class="tab-pane fade in" id="anonymous">
          {{ Form::open(array('url' => 'doAnonymous')) }}
          <h1 class="popBox">Anonymous access</h1>
        <p class="popBox">Continue as Anonymous user. No personal information will be requested.</p>

        <p class="popBox"><br>By pressing the button below you adhere to our anonymous usage <a href="/policy" onclick="basicPopup(this.href);return false">policy</a></p>
        
        

        <p class="popBox">{{ Form::submit('Submit') }}</p> 
        {{ Form::close() }}
        </div>
        <div class="tab-pane fade active in" id="signin">
          
             {{ Form::open(array('url' => 'login')) }}
    <h1 class="popBox">Sign In</h1>

    <!-- if there are login errors, show them here -->
    <p>
      {{ $errors->first('email') }}
      {{ $errors->first('password') }}
    </p>

    <p class="popBox">
      {{ Form::label('email', 'Email:') }}</p><p class="popBox">
      {{ Form::text('email', Input::old('email'), array('placeholder' => 'richard@matieswiki.com')) }}
    </p>

    <p class="popBox">
      {{ Form::label('password', 'Password:') }}</p><p class="popBox">
      {{ Form::password('password', array('placeholder' => '********')) }}
    </p>   
    <p class="popBox">{{ Form::submit('Submit') }}</p>
  {{ Form::close() }}
    
        </div>
        <div class="tab-pane fade" id="signup">
            
    {{ Form::open(array('url' => 'register')) }}
    <h1 class="popBox">Register</h1>
        
        <p class="popBox">{{ Form::label('Username:') }}</p>
        
 
        <p class="popBox">{{ Form::text('username') }}</p>
 
        <p class="popBox">{{ Form::label('Email:') }}</p>
 
        <p class="popBox">{{ Form::text('email') }}</p>
 
        <p class="popBox">{{ Form::label('Password:') }}</p>
 
        <p class="popBox">{{ Form::password('password') }}</p>

        <p class="popBox">{{ Form::submit('Submit') }}</p> 

 
    {{ Form::close() }}
      </div>

    </div>
      </div>
      <div class="modal-footer">
      <center>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </center>
      </div>
    </div>
  </div>
</div>


<div class="container">	

<!--{Session::put('prev', URL::full());}} -->
<div align="center">
@if (isset(Auth::user()->username))
	<div class="alert alert-info">
	Logged in user: {{Auth::user()->username}}
	</div>
@elseif (Session::has('message'))
	<div class="alert alert-info">
	{{ Session::get('message') }} <br>
	<a href="{{ URL::to('login') }}"> Login</a> or <a href="{{ URL::to('register') }}"> Register</a>
	</div>
@else
	<div class="alert alert-info">
	Not Logged in, <br>
	please click on <a href="{{ URL::to('login') }}"> Login</a> or <a href="{{ URL::to('register') }}"> Register</a>
	</div>
@endif

</div>

<h1 align="center">Search Results For:</h1>
<h2 align="center"> "{{ $keyword }}"</h2>


<table class="results-left-table">
  <td valign="top">
@if ($titles != null)
<h4>Titles</h4>
@foreach($titles as $title)
    
      <p> <a href="{{ URL::to('wiki/show/' . $title->id) }}">{{ $title->title }}</a></p>

@endforeach
@else
None
@endif
</td>
</table>


<table class="results-right-table">
  <td valign="top">
@if ($tags != null)
<h4>Tags</h4>
@foreach($tags as $tag)
    
      <p> <a href="{{ URL::to('wiki/resultsTags/' . $tag->id) }}">{{ $tag->tag }}</a></p>

@endforeach
@endif

</td>
</table>

<table class="results-center-table" align="center">
  <td valign="top">
@if ($tags != null)
<h4>Categories</h4>
@foreach($categories as $category)
    
      <p><a href="{{ URL::to('wiki/results/' . $category->id) }}">{{ $category->name }}</a></p>

@endforeach
@endif

</td>
</table>

</body>
</html>


