<!doctype html>
<html lang="en">
<head>
    <title>Stellenbosch Wiki/Register</title>
</head>
<body>
 
    {{ Form::open(array('url' => 'register')) }}
 
        <p>Username :</p>
 
        <p>{{ Form::text('username') }}</p>
 
        <p>Email :</p>
 
        <p>{{ Form::text('email') }}</p>
 
        <p>Password :</p>
 
        <p>{{ Form::password('password') }}</p>

        <p>Anonymous:</p>
        <p> {{Form::checkbox('anonymous_checkbox', '1')}}</p>

        <p>{{ Form::submit('Submit') }}</p> 

 
    {{ Form::close() }}
 
</body>
</html>