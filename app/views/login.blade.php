<!doctype html>
<html>
<head>
	<title>Stellenbosch Wiki/Login</title>
</head>
<body>

@if (isset(Auth::user()->username))
	<div class="alert alert-info">
	Already logged in as user: {{Auth::user()->username}}
	</div>
@elseif (Session::has('message'))
	<div class="alert alert-info">
	{{ Session::get('message') }} <br>
	<a href="{{ URL::to('login') }}"> Login</a> or <a href="{{ URL::to('register') }}"> Register</a>
	</div>
@else
	<div class="alert alert-info">
	Not Logged in, <br>
	please click on <a href="{{ URL::to('login') }}"> Login</a> or <a href="{{ URL::to('register') }}"> Register</a>
	</div>
@endif

	{{ Form::open(array('url' => 'login')) }}
		<h1>Login</h1>

		<!-- if there are login errors, show them here -->
		<p>
			{{ $errors->first('email') }}
			{{ $errors->first('password') }}
		</p>

		<p>
			{{ Form::label('email', 'Email') }}
			{{ Form::text('email', Input::old('email'), array('placeholder' => 'richard@matieswiki.com')) }}
		</p>

		<p>
			{{ Form::label('password', 'Password') }}
			{{ Form::password('password') }}
		</p>

		<p>Anonymous:</p>
        <p> {{Form::checkbox('anonymous_checkbox', '1')}}</p>
        <p><a href="{{ URL::to('register') }}">Register:</a>      
		<p>{{ Form::submit('Submit!') }}</p>
	{{ Form::close() }}


	

</body>
</html>
