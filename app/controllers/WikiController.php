<?php
use \Michelf\Markdown;
class WikiController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{


		/**if (Auth::check())
		{
			$user = Auth::user()->username;
			return $user;
		}*/
		//get all information from the page table
		$wiki = Page::all();
		$categories = Category::all();
		//$category_id = DB::table('pages')->where('id', $id)->pluck('categories_id');

		return View::make('wiki.index')->with('pages', $wiki)->with('category', $categories); 



	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		

		return View::make('wiki.create');


	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		// validation
		$rules = array(
			'name'       => 'required',
			'title'      => 'required',
			//'section'    => 'required',
			//'order' => 'required',
			'content'   => 'required'

		);
		$validator = Validator::make(Input::all(), $rules);

		// process the login
		
		if ($validator->fails()) {
			return Redirect::to('wiki/create')
				->withErrors($validator)
				->withInput(Input::except('password'));

		} else {

				if (Auth::check())
			{
				$user = Auth::user()->id;
			
			} 

					// store
					$category = new Category;
					$category->name = Input::get('name');
					$name = $category->name;
					

					

					$categories_id = DB::table('categories')->where('name', $name)->pluck('id');


					$page = new Page;
					$page->title   = Input::get('title');
					$page->categories_id = $categories_id;
					$page->users_id       = $user;
					$page->save();


					$content = new Content;
					$content->section  = Input::get('section');
					$content->order    = Input::get('order');
					$content->content  = Input::get('content');
					$content->users_id = $user;
					$content->pages_id = $page->id;
					$content->save();

					$tag = new Tag;
					$tag->tag   = Input::get('tag');
					$tag->pages_id   = $page->id;
					$tag->save();

						// redirect
					Session::flash('message', 'Successfully created new page');
					return Redirect::to('wiki');
				}

			
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		// get the page
		$wiki = Page::find($id);
		$users_id = DB::table('pages')->where('id', $id)->pluck('users_id');
		$author = DB::table('users')->where('id', $users_id)->pluck('username');
		$category_id = DB::table('pages')->where('id', $id)->pluck('categories_id');
		$category = DB::table('categories')->where('id', $category_id)->pluck('name');

		$latest_content = DB::table('contents')->where('pages_id', $id)->orderBy('updated_at', 'desc')->pluck('content');
		

        $content = Content::find($id);
        $markedDownContent = Markdown::defaultTransform($latest_content);
        //var_dump($markedDownContent);
        $tags = DB::table('tags')->where('pages_id', $id)->get();
        $user = DB::table('contents')->where('pages_id', $id)->lists('users_id');
        $contentIDs = DB::table('contents')->where('pages_id', $id)->lists('id');

        $count=0;
        $editorsArray =array();
        //$updateTimestampArray = array();

        foreach ($user as $usr) {
        	$editors = DB::table('users')->where('id', $user[$count])->pluck('username');
        	$editorsArray[$count] = $contentIDs[$count].", ".$editors.", ".DB::table('contents')->where('id', $user[$count])->pluck('updated_at');
        	; 
        	$count++;
        }
        //$editors = DB::table('users')->where('id', $user[1])->lists('username');


        
		// show the view and pass the page to it
		return View::make('wiki.show')
		->with('page', $wiki)
		->with('author', $author)
		->with('category', $category)
		->with('content', $content)
		->with('tags', $tags)
		->with('editors', $editorsArray)
		->with('latest', $markedDownContent); 

	}

		public function showRevisions($id)
	{
		//var_dump($id);
		// get the page
		//$wiki = Page::find($id);
		$users_id = DB::table('contents')->where('id', $id)->pluck('users_id');
		$author = DB::table('users')->where('id', $users_id)->pluck('username');

		//$category_id = DB::table('pages')->where('id', $id)->pluck('categories_id');
		//$category = DB::table('categories')->where('id', $category_id)->pluck('name');
		

		$viewingContent = DB::table('contents')->where('id', $id)->pluck('content');
		$markedDownContent = Markdown::defaultTransform($viewingContent);
		//var_dump($viewingContent);

        //$content = Content::find($conId);
        //$tags = DB::table('tags')->where('pages_id', $id)->get();
                
		// show the view and pass the page to it
		return View::make('wiki.revisionShow')
		->with('contentID', $id)
		->with('author', $author)
		->with('content', $markedDownContent);

	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		// get the page
		$wiki = Page::find($id);
		$title = $wiki->title;
		$category_id = DB::table('pages')->where('id', $id)->pluck('categories_id');
		$category = DB::table('categories')->where('id', $category_id)->pluck('name');
		$cont = Content::find($id);
		//$content = $cont->content;
		$content = DB::table('contents')->where('pages_id', $id)->orderBy('updated_at', 'desc')->pluck('content');
		$all = array('id'=>$id, 'title' => $title,'name' => $category, 'content' => $content);
		
		

		// show the edit form and pass the page
		return View::make('wiki.edit', $all)
			->with('all', $all);

	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
				// validation
				$rules = array(
			'name'       => 'required',
			'title'      => 'required',
			'content' => 'required'
		);
		$validator = Validator::make(Input::all(), $rules);

		// process the login
		if ($validator->fails()) {
			return Redirect::to('wiki/edit/' . $id)
				->withErrors($validator)
				->withInput(Input::except('password'));
		} else {

				if (Auth::check())
			{


				$user = Auth::user()->id;
			
			} 

			//store
			
					
		            $category_id = DB::table('pages')->where('id', $id)->pluck('categories_id');
		            $category = DB::table('categories')->where('id', $category_id)->pluck('name');
					
					

					

					$categories_id = DB::table('categories')->where('name', $category)->pluck('id');

					$author = DB::table('pages')->where('id', $id)->pluck('users_id');
				    //$author = DB::table('users')->where('id', $users_id)->pluck('username');

				    //var_dump($author);


					$page = Page::find($id);
					$page->title   = Input::get('title');
					$page->categories_id = $categories_id;
					$page->users_id       = $author;
					$page->save();


					$content = new Content;
					$content->section  = Input::get('section');
					$content->order    = Input::get('order');
					$content->content  = Input::get('content');
					$content->users_id = $user;
					$content->pages_id = $page->id;
					$content->save();

					$tag = new Tag;
					$tag->tag   = Input::get('tag');
					$tag->pages_id   = $page->id;
					$tag->save();

						// redirect
					Session::flash('message', 'Successfully edited page');
					return Redirect::to('wiki/show/' . $id);
				}
		

	}

	public function revisionUpdate($id)
	{
					$contents = DB::table('contents')->where('id', $id)->pluck('content');
					$users = DB::table('contents')->where('id', $id)->pluck('users_id');
					$pages = DB::table('contents')->where('id', $id)->pluck('pages_id');
					$contentID = DB::table('contents')->where('id', $id)->pluck('id');

					DB::table('contents')->where('id', $contentID)->delete();


					$content = new Content;
					$content->id = $contentID;
					$content->content  = $contents;
					$content->users_id = $users;
					$content->pages_id = $pages;
					$content->save();

					Session::flash('message', 'Successfully rolled back page');
					return Redirect::to('wiki/show/' . $pages);

	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
