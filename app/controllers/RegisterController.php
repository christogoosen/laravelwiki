<?php

class RegisterController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return View::make('register');
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//$anonomousStatus = Input::get('anonomous_checkbox');
		// validation
		//if(Input::get('anonymous_checkbox') !=='1')
		//{
			//checks whether user is anonymous
			//without check anonymous user would have to fill in form, defeating purpose
		$rules = array(
			'username'       => 'required|alphaNum|min:2|unique:users',
			'email'      => 'required|email|unique:users',
			'password'    => 'required|alphaNum|min:3'
			

		);
		$validator = Validator::make(Input::all(), $rules);
		
		// process the login
		
		if ($validator->fails()) {


			Session::flash('message', 'Registration failed');
			Session::flash('message', 'Credentials entered incorrectly');
			return Redirect::to('wiki')
			//echo "Failed registration";

				->withErrors($validator)
				->withInput(Input::except('password'));

		} else {
			//creates new registered user
			// store
			
			$role_id = DB::table('roles')->where('name', 'registered')->pluck('id');

			$user = new User;
			$user->username = Input::get('username');
			$user->password = Input::get('password');
			$user->email = Input::get('email');
			$user->roles_id = $role_id;
			$user->ip =$_SERVER['REMOTE_ADDR'];
			//$user->ip =$_SERVER['HTTP_CLIENT_IP'];
		

			// hash the password
		    $user['password'] = Hash::make($user['password']);
			$user->save();
			//Logs created user in after registration
			Auth::login($user);

				// redirect
			Session::flash('message', 'Successfully registered');
			return Redirect::to('wiki');
		}
		//}
		//else
		//{
			//RegisterController::createAnonymous();
			//return Redirect::to('wiki');
		//}

	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */

	public function createAnonymous()
	{
		//creating a anonymous user

			$role_id = DB::table('roles')->where('name', 'anonymous')->pluck('id');

			
						
			$user = new User;
			$count = 0;
			$searchTerm = 'Anonymous';
			$userz = DB::table('users')->where('email','')->get();
			foreach ($userz as $a)
			{
			    //var_dump($user->username);
			    $count ++;
			}
			//assigning anonymous details, tracks external IP
			$count += 1;
			$user->username = 'Anonymous'.$count;
			$user->password ='anon';
			$user->email = '';
			$user->roles_id = $role_id;
			$user->ip =$_SERVER['REMOTE_ADDR'];
			//$user->ip =$_SERVER['HTTP_CLIENT_IP'];
		

			// hash the password
		    $user['password'] = Hash::make($user['password']);
			$user->save();
			//logs in the created user (Anon)

			Auth::login($user);

				Session::flash('message', 'Access As Anonymous!');
				return Redirect::to('wiki');

			//Session::flash('message', 'Anonymous user');

	}
	public function show($id)
	{
		//
	}
	


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
