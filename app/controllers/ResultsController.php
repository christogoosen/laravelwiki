<?php

class ResultsController extends \BaseController {

	public function getResults($id) 
	{
        
		$title = DB::table('pages')->where('categories_id', $id)->get();

		return View::make('wiki/results.categories')
		->with('title', $title);
	

	}

	public function getTags($id)
	{
		$tags = DB::table('tags')->where('id', $id)->pluck('pages_id');
		$pages = DB::table('pages')->where('id', $tags)->distinct()->get();

		return View::make('wiki/results.tags')
		->with('pages', $pages); 
	}


	public function getSearch()
	{
    
          return View::make('search');
	}

	public function doSearch()
	{

		// validation
		$rules = array(
			'keyword'       => 'required',

		);
		$validator = Validator::make(Input::all(), $rules);

		// process the login
		
		if ($validator->fails()) {
			return Redirect::to('wiki')
				->withErrors($validator)
				->withInput(Input::except('password'));

		} else {


				$keyword = Input::get('keyword');
				$titles = Page::where('title', 'LIKE', '%'.$keyword.'%')->get();
		        $tags = Tag::where('tag', 'LIKE', '%'.$keyword.'%')->get();
		        $categories = Category::where('name', 'LIKE', '%'.$keyword.'%')->get();

          
		        //var_dump($titles);
			    
		 	    return View::make('search')
		 	    ->with('titles', $titles)
		 	    ->with('tags', $tags)
		 	    ->with('categories', $categories)
		 	    ->with('keyword', $keyword);
               }
      }
}