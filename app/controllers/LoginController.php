<?php

class LoginController extends \BaseController {

	public function showLogin()
	{
		// show the form
		return View::make('login');
	}

	//public function doAnonymous()
	//{
		//Route::post('register', array('uses' => 'RegisterController@createAnonymous'));

	//}

	public function doLogin()
	{
		//if(Input::get('anonymous_checkbox') !=='1')
		//{

		// validate the info, create rules for the inputs
		$rules = array(
			
			'password' => 'required|alphaNum|min:3', // password can only be alphanumeric and has to be greater than 3 characters
			'email'    => 'required|email' // make sure the email is an actual email
		);

		// run the validation rules on the inputs from the form
		$validator = Validator::make(Input::all(), $rules);

		// if the validator fails, redirect back to the form
		if ($validator->fails()) {
			Session::flash('message', 'Credentials entered incorrectly');
			return Redirect::to('wiki')
				->withErrors($validator) // send back all errors to the login form
				->withInput(Input::except('password')); // send back the input (not the password) so that we can repopulate the form
		} else {

			// create our user data for the authentication
			$userdata = array(
				'password' 	=> Input::get('password'),
				'email' 	=> Input::get('email')
				
			);

			// attempt to do the login
			if (Auth::attempt($userdata)) {

				// validation successful!
				// redirect them to the secure section or whatever
				// return Redirect::to('secure');
				// for now we'll just echo success (even though echoing in a controller is bad)
				//echo 'SUCCESS!';
				Session::flash('message', 'Logged In!');
				return Redirect::to('wiki');

			} else {	 	

				// validation not successful, send back to form	
				//return Redirect::to('login');

			}



		}
	//}
	//else
	//{
	//	LoginController::doAnonymous();
		//return Redirect::to('wiki');
	//}
	}

	public function doLogout()
	{
		if(isset(Auth::user()->username))
		{
		Session::flush();
		Session::flash('message', 'Logged out');
		//Auth::logout(); // log the user out of our application
		return Redirect::to('wiki'); // redirect the user to the login screen
		}
		else
		{
			return Redirect::to('wiki'); // redirect the user to the login screen
		}
	}


} 