<?php


class Page extends Eloquent {
	
	protected $table = 'pages';

	//protected $fillable = array('title');


	public function tags() {
		return $this->hasMany('Tag'); // this matches the Eloquent model
	}

	public function contents() {
		return $this->hasMany('Content'); // this matches the Eloquent model
	}

	public function categories() {
		return $this->belongsTo('Category');
	}

	public function users() {
		return $this->belongsTo('User');
	}



}