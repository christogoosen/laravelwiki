<?php


class Category extends Eloquent {
	protected $table = 'categories';


	//protected $fillable = array('name');

	public function pages() {
		return $this->hasMany('Page'); // this matches the Eloquent model
	}


}