<?php


class Content extends Eloquent {
	
	protected $table = 'contents';

	//protected $fillable = array();
	
	public function pages() {
		return $this->belongsTo('Page');
	}

	public function users() {
		return $this->belongsTo('User');
	}
}
