<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;


	protected $table = 'users';
	protected $hidden = array('password');

	

	public function pages() {
		return $this->hasOne('Page'); // this matches the Eloquent model
	}

	public function contents() {
		return $this->hasMany('Content'); // this matches the Eloquent model
	}

	public function roles() {
		return $this->belongsTo('Role');

	}

 


}
