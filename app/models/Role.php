<?php


class Role extends Eloquent {
	
	protected $table = 'roles';

	//protected $fillable = array('title');
	
	public function users() {
		return $this->hasMany('User'); // this matches the Eloquent model
	}
}