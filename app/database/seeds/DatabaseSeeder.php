<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		// call our class and run seeds
		$this->call('WikiSeeder');
		$this->command->info('Succesfully finished seeding');
	}

}

class WikiSeeder extends Seeder {

	public function run() {

		DB::table('pages')->delete();
		DB::table('categories')->delete();
		DB::table('contents')->delete();
		DB::table('users')->delete();
		DB::table('roles')->delete();
		DB::table('tags')->delete();


		//create entries
		
		$rolesRegistered = Role::create(array(
          	'name'  => 'registered'
          	));

		$rolesAnonymous = Role::create(array(
			'name' => 'anonymous'));

		$rolesAdmin = Role::create(array(
			'name' => 'admin'));

           $this->command->info('roles done');

		$usersNew = User::create(array(
         	'username' => 'wesley',
         	'password' => Hash::make('zomgay101'),
         	'email' => 'wesfet@gmail.com',
         	'roles_id' => $rolesRegistered->id,
         	'ip'  => '192.168.0.52'

         	));

          $this->command->info('users done');


		$categoriesFaculty = Category::create(array(
			'name'  =>  'Faculty'

			));

		$categoriesCourse = Category::create(array(
			'name'  =>  'Course'

			));

		
		$categoriesSubject = Category::create(array(
			'name'  =>  'Subject'

			));

		$categoriesModule = Category::create(array(
			'name'  =>  'Module'

			));





        $this->command->info('categories done');
		
		$pageEnglish = Page::create(array(
			'title'  => 'English114',
			'categories_id'  => $categoriesModule->id,
			'users_id'  => $usersNew->id

			));

		$this->command->info('pages done');


        $contentsNew = Content::create(array(
        	'section' => 'Introduction',
        	'order'  =>   1,
        	'content' => 'English114 is not the easiest module around town',
        	'users_id'  =>  $usersNew->id,
        	'pages_id'  => $pageEnglish->id

        	));

         $this->command->info('content done');

         

          

           $tagsNew = Tag::create(array(
           	'tag'  => 'language',
           	'pages_id'  => $pageEnglish->id

           	));

            $this->command->info('tags done');

	}

}
