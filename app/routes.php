<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('hello');
});

Route::post('search', array('uses' => 'ResultsController@doSearch'));

Route::get('wiki/results/{id}', 'ResultsController@getResults');

Route::get('wiki/resultsTags/{id}', 'ResultsController@getTags');

Route::get('wiki/show/{id}', 'WikiController@show');

Route::get('wiki/edit/{id}', 'WikiController@edit');

Route::get('viewRevise/{id}', 'WikiController@showRevisions');

Route::get('rollback/{id}', 'WikiController@revisionUpdate');

Route::resource('wiki', 'WikiController');

Route::post('doAnonymous', array('uses' => 'RegisterController@createAnonymous'));

Route::resource('register', 'RegisterController');

Route::post('register', array('uses' => 'RegisterController@store'));

	// route to show the login form
Route::get('login', array('uses' => 'LoginController@showLogin'));

	// route to process the form
Route::post('login', array('uses' => 'LoginController@doLogin'));

Route::get('logout', array('uses' => 'LoginController@doLogout'));

Route::get('/testView', function()
{
	return View::make('testView');
});

Route::get('/policy', function()
{
	return View::make('policy');
});
